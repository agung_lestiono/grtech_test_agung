@extends('layouts.master')

@section('style')
@parent

@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Detail Company
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"> Home</a></li>
            <li><a href="/"> Companies</a></li>
            <li class="active"> Detail Company</li>
        </ol>
    </section>
    @if(Session::has('message')) 
    <section class="content-header">
        {!! Session::get('message') !!}
    </section>
    @endif
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            
            <div class="box-header with-border">
                <a href="/companies" class="btn btn-danger"> Back</a>
                
            </div>
            <div class="box-body">
                {{ Form::hidden('id', $id, ['id'=>'id']) }}
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Company Name</label>
                    <div class="col-sm-10">
                        {{ Form::text('name', $name, ['class'=>'form-control', 'id'=>'name', 'readonly']) }}
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        {{ Form::text('email', $email, ['class'=>'form-control','id'=>'email', 'readonly']) }}
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Logo</label>
                    <div class="col-sm-10">
                       <img src="{{ asset('storage/'.$logo) }}" alt="" title="" width="250px"></a>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Website</label>
                    <div class="col-sm-10">
                        {{ Form::text('website', $website, ['class'=>'form-control','id'=>'website', 'readonly']) }}
                    </div>
                </div>

                <div class="box-footer">

                </div>
                
            </div>

        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('js')
@parent

@endsection