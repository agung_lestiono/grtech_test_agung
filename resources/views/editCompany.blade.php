@extends('layouts.master')

@section('style')
@parent

@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Company
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"> Home</a></li>
            <li><a href="/"> Companies</a></li>
            <li class="active"> Edit Company</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            {{ Form::open(['url' => '/companies/update/', 'files'=> true]) }}
            <div class="box-header with-border">
                <a href="/companies/show/{{ $id }}" class="btn btn-danger"> Back</a>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
            <div class="box-body">
                {{ Form::hidden('id', $id, ['id'=>'id']) }}
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Company Name</label>
                    <div class="col-sm-10">
                        {{ Form::text('name', $name, ['class'=>'form-control', 'id'=>'name', 'required']) }}
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        {{ Form::text('email', $email, ['class'=>'form-control','id'=>'email', 'required']) }}
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Logo</label>
                    <div class="col-sm-10">
                        <img src="{{ asset('storage/'.$logo) }}" alt="" title="" width="250px"></a>
                        {{ Form::file('image') }}
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Website</label>
                    <div class="col-sm-10">
                        {{ Form::text('website', $website, ['class'=>'form-control','id'=>'website', 'required']) }}
                    </div>
                </div>

                <div class="box-footer">

                </div>
                
            </div>
            {{ Form::close() }}
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('js')
@parent

@endsection