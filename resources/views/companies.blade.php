@extends('layouts.master')

@section('style')
@parent

@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Company List
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"> Home</a></li>
            <li class="active">Companies</li>
        </ol>
    </section>
    @if(Session::has('message')) 
    <section class="content-header">
        {!! Session::get('message') !!}
    </section>
    @endif

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <div style="width: 100px">
                    <a href="/companies/create" class="btn btn-block btn-warning"> <i class="fa fa-fw fa-plus"></i> Company</a>    
                </div>
            </div>
            <div class="box-body">
                <table class="table table-bordered data-table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Logo</th>
                            <th>Website</th>
                            <th width="100px">Action</th>
                        </tr>

                    </thead>

                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
            <!-- /.box -->
    </section>
        <!-- /.content -->
</div>
    <!-- /.content-wrapper -->

<div class="modal fade" id="edit-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Company</h4>
            </div>
                <div class="modal-body">
                    {{ Form::open(['url' => '/companies/update/', 'files'=> true]) }}
                    <div class="box-body">
                        {{ Form::hidden('id', '', ['id'=>'id']) }}
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Company Name</label>
                            <div class="col-sm-10">
                                {{ Form::text('name', '', ['class'=>'form-control', 'id'=>'name', 'required']) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                                {{ Form::text('email', '', ['class'=>'form-control','id'=>'email', 'required']) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Logo</label>
                            <div class="col-sm-10">
                                {{ Form::file('image') }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Website</label>
                            <div class="col-sm-10">
                                {{ Form::text('website', '', ['class'=>'form-control','id'=>'website', 'required']) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="update-btn">Update</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    @parent
    <script type="text/javascript">
        $(function () {
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ url('/companies') }}",
                columns: [
                    {data: 'no', name: 'no'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'new_logo', name: 'new_logo'},
                    {data: 'new_website', name: 'new_website'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
        });
    </script>
    <script src="/js/company.js"></script>
@endsection