@extends('layouts.master')

@section('style')
@parent
<link rel="stylesheet" href="/bower_components/select2/dist/css/select2.min.css">
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Employee List
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"> Home</a></li>
            <li class="active">Employee</li>
        </ol>
    </section>
    @if(Session::has('message')) 
    <section class="content-header">
        {!! Session::get('message') !!}
    </section>
    @endif
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <div style="width: 100px">
                    <a href="/employees/create" class="btn btn-block btn-warning"> <i class="fa fa-fw fa-plus"></i> Employee</a>    
                </div>
            </div>
            <div class="box-body">
                <table class="table table-bordered data-table" id="employee-table">
                    <thead>
                        <tr>
                            <th width="10%">#</th>
                            <th>Employee Name</th>
                            <th>Company</th>
                            <th width="100px">Action</th>
                        </tr>

                    </thead>

                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>

<div class="modal fade" id="edit-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Employee</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => '/employees/update/' ]) }}
                    {{ Form::hidden('id', '', ['id'=>'id']) }}
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">First Name</label>
                        <div class="col-sm-10">
                            {{ Form::text('first_name', '', ['class'=>'form-control', 'id'=>'first-name', 'required']) }}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Last Name</label>
                        <div class="col-sm-10">
                            {{ Form::text('last_name', '', ['class'=>'form-control', 'id'=>'last-name', 'required']) }}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Company</label>
                        <div class="col-sm-10">
                            {{ Form::select('company_id', $company_list, '', ['class'=>'form-control select2','id'=>'company-id', 'required']) }}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                            {{ Form::text('email', '', ['class'=>'form-control','id'=>'email', 'required']) }}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Phone</label>
                        <div class="col-sm-10">
                            {{ Form::text('phone', '', ['class'=>'form-control','id'=>'phone', 'required']) }}
                        </div>
                    </div>

                    <div class="box-footer">

                    </div>
                    
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="update-btn">Update</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

@endsection

@section('js')
    @parent
    <script src="/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script type="text/javascript">
        $(function () {
            var table = $('#employee-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ url('/employees') }}",
                columns: [
                {data: 'no', name: 'no'},
                {data: 'fullname', name: 'fullname'},
                {data: 'company', name: 'company', orderable: false, searchable: false},
                {data: 'action', name: 'action', orderable: false, searchable: false},
                ]

            });
        });
    </script>
    <script src="/js/employee.js"></script>
@endsection