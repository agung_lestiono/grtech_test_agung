@extends('layouts.master')

@section('style')
@parent


@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Detail Employee
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"> Home</a></li>
            <li><a href="/employees"> Employees</a></li>
            <li class="active"> Detail Employee</li>
        </ol>
    </section>
    @if(Session::has('message')) 
    <section class="content-header">
        {!! Session::get('message') !!}
    </section>
    @endif
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            
            <div class="box-header with-border">
                <a href="/employees" class="btn btn-danger"> Back</a>
                <a href="/employees/edit/{{ $id }}" class="btn btn-primary"> Edit</a>
            </div>
            <div class="box-body">
                {{ Form::hidden('id', $id, ['id'=>'id']) }}
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">First Name</label>
                    <div class="col-sm-10">
                        {{ Form::text('first_name', $first_name, ['class'=>'form-control', 'id'=>'first-name', 'readonly']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Last Name</label>
                    <div class="col-sm-10">
                        {{ Form::text('last_name', $last_name, ['class'=>'form-control', 'id'=>'last-name', 'readonly']) }}
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Company</label>
                    <div class="col-sm-10">
                        {{ Form::select('company_id', $company_list, $company_id, ['class'=>'form-control select2','id'=>'company-id', 'disabled']) }}
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        {{ Form::text('email', $email, ['class'=>'form-control','id'=>'email', 'readonly']) }}
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Phone</label>
                    <div class="col-sm-10">
                        {{ Form::text('phone', $phone, ['class'=>'form-control','id'=>'phone', 'readonly']) }}
                    </div>
                </div>

                <div class="box-footer">

                </div>
                
            </div>
            
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('js')
@parent

@endsection