@extends('layouts.master')

@section('style')
@parent
<link rel="stylesheet" href="/bower_components/select2/dist/css/select2.min.css">
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    @if(Session::has('message'))
            {!! Session::get('message') !!}
        @endif
    <section class="content-header">
        <h1>
            Create New Employee
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"> Home</a></li>
            <li><a href="/employees"> Employees</a></li>
            <li class="active"> Create New Employee</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            {{ Form::open(['url' => '/employees/store']) }}
            <div class="box-header with-border">
                <a href="/employees" class="btn btn-danger"> Back</a>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <div class="box-body">
                {{ Form::hidden('id', $id, ['id'=>'id']) }}
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">First Name</label>
                    <div class="col-sm-10">
                        {{ Form::text('first_name', $first_name, ['class'=>'form-control', 'id'=>'first-name', 'required']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Last Name</label>
                    <div class="col-sm-10">
                        {{ Form::text('last_name', $last_name, ['class'=>'form-control', 'id'=>'last-name', 'required']) }}
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Company</label>
                    <div class="col-sm-10">
                        {{ Form::select('company_id', $company_list, $company_id, ['class'=>'form-control select2','id'=>'company-id', 'required']) }}
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        {{ Form::text('email', $email, ['class'=>'form-control','id'=>'email', 'required']) }}
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Phone</label>
                    <div class="col-sm-10">
                        {{ Form::text('phone', $phone, ['class'=>'form-control','id'=>'phone', 'required']) }}
                    </div>
                </div>

                <div class="box-footer">

                </div>
                
            </div>
            {{ Form::close() }}
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('js')
@parent
<script src="/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="/js/employee.js"></script>
@endsection