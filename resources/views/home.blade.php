@extends('layouts.master')

@section('style')
    @parent
    <link rel="stylesheet" href="/bower_components/morris.js/morris.css">
@endsection

@section('content')
    <div class="content-wrapper">
        <section class="content">

            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <img src="/img/home.jpg" class="col-sm-6" style="margin-left: 25%">
                    </div>
                </div>
                <div class="box-footer">
                </div>
            </div>
        </section>
    </div>

@endsection

@section('js')
    @parent
    
@endsection