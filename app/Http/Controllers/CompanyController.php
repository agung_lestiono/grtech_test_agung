<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Company;
use DataTables;
use DateTime;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Company::all();

            foreach ($data as $key => $value) {
                $data[$key]->no = $key + 1;
            }

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('new_logo', function($row){
                        $logo = '<img src="/storage/' . $row->logo . '" alt="" title="" width="250px"></a>';

                        return $logo;
                    })
                    ->addColumn('new_website', function($row){
                        $website = '<a href="' . $row->website . '" target="new">'. $row->website .'</a>';

                        return $website;
                    })
                    ->addColumn('action', function($row){
                        $btn = '<a href="#edit-modal" class="edit-btn btn btn-primary btn-sm" data-toggle="modal" data-target="#edit-modal" data-company-id="' . $row->id .'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                        $btn .= '<a href="/companies/destroy/' . $row->id . '" class="edit btn btn-danger btn-sm" onclick="if(!confirm(\'Are you sure want to delete this company?\')) return false;"> <i class="fa fa-trash" aria-hidden="true"></i></a>';

                        return $btn;
                    })
                    
                    ->rawColumns(['action', 'new_logo', 'new_website'])
                    ->make(true);

        }
        return view('companies');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('createCompany', [
            'page_title'    => 'Create Company',
            'id'            => '',
            'name'          => '',
            'email'         => '',
            'logo'          => '',
            'website'       => ''
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('image');
        $name = rand();
        $extention = $file->getClientOriginalExtension();
        $new_name = $name . '.' . $extention;
        
        $path = Storage::putFileAs('public', $request->file('image'), $new_name);

        $company = New Company;
        $company->name = $request->name;
        $company->email = $request->email;
        $company->logo = $new_name;
        $company->website = $request->website;
        $company_store = $company->save();

        if ($company_store) {
            return redirect ('/companies/')->with('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Company is successfully.</div>');
        }
        else {
            return redirect ('/companies/')->with('message', '<div class="alert alert-danger alert-dismissible"> Failed.</div>');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::find($id);

        return view('showCompany', [
            'page_title'    => $company->name . ' Detail',
            'id'            => $company->id,
            'name'          => $company->name,
            'email'         => $company->email,
            'logo'          => $company->logo,
            'website'       => $company->website
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);

        // return view('editCompany', [
        //     'page_title'    => $company->name . ' Detail',
        //     'id'            => $company->id,
        //     'name'          => $company->name,
        //     'email'         => $company->email,
        //     'logo'          => $company->logo,
        //     'website'       => $company->website
        //     ]
        // );

        return response()->json($company);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $company = Company::find($request->id);
        $file = $request->file('image');

        if ($file) {
            $name = rand();
            $extention = $file->getClientOriginalExtension();
            $new_name = $name . '.' . $extention;
        
            $path = Storage::putFileAs('public', $request->file('image'), $new_name);
            $company->logo = $new_name;
        }
        
        $company->name = $request->name;
        $company->email = $request->email;
        
        $company->website = $request->website;
        $company_store = $company->save();

        if ($company_store) {
            return redirect ('/companies/')->with('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Company is updated successfully.</div>');
        }
        else {
            return redirect ('/companies/edit/' . $company->id)->with('message', '<div class="alert alert-danger alert-dismissible"> Failed!</div>');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::find($id);
        $company_destroy = $company->delete();

        if ($company_destroy) {
            return redirect ('/companies')->with('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Company is deleted successfully.</div>');
        }
        else {
            return redirect ('/linen')->with('message', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed!</div>');
        }
    }
}
