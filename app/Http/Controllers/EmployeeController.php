<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Company;
use DataTables;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Employee::all();

            foreach ($data as $key => $value) {
                $data[$key]->no = $key + 1;
            }
            
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('fullname', function($row){
                        $fullname = $row->full_name;
                        return $fullname;
                    })

                    ->addColumn('company', function($row){
                        $company = Company::find($row->company_id);
                        $company_link = '<a href="/companies/show/' . $row->company_id . '" target="new">' . $company->name . '</a>';

                        return $company_link;
                    })

                    ->addColumn('action', function($row){
                        $btn = '<a href="#edit-modal" class="edit-btn btn btn-primary btn-sm" data-toggle="modal" data-target="#edit-modal" data-employee-id="' . $row->id .'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                        
                        $btn .= '<a href="/employees/destroy/' . $row->id . '" class="edit btn btn-danger btn-sm" onclick="if(!confirm(\'Are you sure want to delete this employee?\')) return false;"> <i class="fa fa-trash" aria-hidden="true"></i></a>';

                        return $btn;
                    })
                    ->rawColumns(['company','action'])
                    ->make(true);

        }

        $company_list = [''=>'Select Company'];
        $companies = Company::all();

        foreach ($companies as $key => $comp) {
            $company_list[$comp->id] = $comp->name;
        }
        
        return view('employees', [
            'company_list' => $company_list,
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company_list = [''=>'Select Company'];
        $companies = Company::all();

        foreach ($companies as $key => $comp) {
            $company_list[$comp->id] = $comp->name;
        }

        return view('createEmployee', [
            'id'            => '',
            'first_name'    => '',
            'last_name'     => '',
            'company_list'  => $company_list,
            'company_id'    => '',
            'email'         => '',
            'phone'         => '',
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $employee = new Employee;
        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->company_id = $request->company_id;
        $employee->email = $request->email;
        $employee->phone = $request->phone;
        $employee_store = $employee->save();

        if ($employee_store) {
            return redirect ('/employees/')->with('message', 
                '<div class="alert alert-success alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Employee is added successfully.</div>');
        }
        else {
            return redirect ('/employees/')->with('message', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed.</div>');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = Employee::find($id);

        $company_list = [''=>'Select Company'];
        $companies = Company::all();

        foreach ($companies as $key => $comp) {
            $company_list[$comp->id] = $comp->name;
        }

        return view('showEmployee', [
            'id'            => $employee->id,
            'first_name'    => $employee->first_name,
            'last_name'     => $employee->last_name,
            'company_list'  => $company_list,
            'company_id'    => $employee->company_id,
            'email'         => $employee->email,
            'phone'         => $employee->phone,
            ]
        );

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::find($id);

        $company_list = [''=>'Select Company'];
        $companies = Company::all();

        foreach ($companies as $key => $comp) {
            $company_list[$comp->id] = $comp->name;
        }

        // return view('editEmployee', [
        //     'id'            => $employee->id,
        //     'first_name'    => $employee->first_name,
        //     'last_name'     => $employee->last_name,
        //     'company_list'  => $company_list,
        //     'company_id'    => $employee->company_id,
        //     'email'         => $employee->email,
        //     'phone'         => $employee->phone,
        //     ]
        // );

        return response()->json($employee);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $employee = Employee::find($request->id);
        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->company_id = $request->company_id;
        $employee->email = $request->email;
        $employee->phone = $request->phone;
        $employee_store = $employee->save();

        if ($employee_store) {
            return redirect ('/employees')->with('message', '<div class="alert alert-success alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Employee is updated successfully.</div>');
        }
        else {
            return redirect ('/employees/')->with('message', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed.</div>');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
    {
        $employee = Employee::find($id);
        $employee_destroy = $employee->delete();

        if ($employee_destroy) {
            return redirect ('/employees')->with('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Employee is deleted successfully.</div>');
        }
        else {
            return redirect ('/employees')->with('message', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed!</div>');
        }
    }
}
