<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = "employees";

    public function company()
    {
    	return $this->belongsTo('App\Company');
    }

    protected $fillable = [
        'first_name','last_name', 'email', 'company_id','phone'
    ];

    public function getFullNameAttribute()
    { 
      return "{$this->first_name} {$this->last_name}";
    }
}
