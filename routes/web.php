<?php

use Illuminate\Support\Facades\Route;
use App\Company;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if(!Auth::user())
    	return view('auth.login');
    else
    	return redirect('/home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->middleware('auth');
Route::get('/companies', 'CompanyController@index')->middleware('auth', 'admin');
Route::get('/companies/create', 'CompanyController@create')->middleware('auth', 'admin');
Route::post('/companies/store', 'CompanyController@store')->middleware('auth', 'admin');
Route::get('/companies/show/{id}', 'CompanyController@show')->middleware('auth', 'admin');
Route::get('/companies/edit/{id}', 'CompanyController@edit')->middleware('auth', 'admin');
Route::post('/companies/update/', 'CompanyController@update')->middleware('auth', 'admin');
Route::get('/companies/destroy/{id}', 'CompanyController@destroy')->middleware('auth', 'admin');

Route::get('/employees', 'EmployeeController@index')->middleware('auth', 'admin');
Route::get('/employees/create', 'EmployeeController@create')->middleware('auth', 'admin');
Route::post('/employees/store', 'EmployeeController@store')->middleware('auth', 'admin');
Route::get('/employees/show/{id}', 'EmployeeController@show')->middleware('auth', 'admin');
Route::get('/employees/edit/{id}', 'EmployeeController@edit')->middleware('auth', 'admin');
Route::post('/employees/update/', 'EmployeeController@update')->middleware('auth', 'admin');
Route::get('/employees/destroy/{id}', 'EmployeeController@destroy')->middleware('auth', 'admin');


