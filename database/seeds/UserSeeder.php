<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(array(
        	'name' => 'admin.login',
        	'email' => 'admin@grtech.com.my',
        	'password' => Hash::make('password')
        ));

		User::create(array(
			'name' => 'user.login',
        	'email' => 'user@grtech.com.my',
        	'password' => Hash::make('password')
        ));        
    }
}
