$(document).ready(function() {
	$('.data-table').on('click', '.edit-btn', function(e) {
    	$.get('/companies/edit/' + $(this).data('company-id'), function (data) {
    		$('#id').val(data.id);
    		$('#name').val(data.name);
    		$('#email').val(data.email);
    		$('#website').val(data.website);
    	})
    })    
});