$(document).ready(function () {
	if ($('#user-list').length) {
		var user_list = $('#user-list').DataTable({
			'ajax' : {
				'url' : '/users/list',
				dataSrc : ''
			},
			'columnsDef' : [
				
			],
			'columns' : [
				{data:'no'},
				{data:'username'},
				{data:'email'},
				{data:'fullname'},
				{data:'action'}
			]
		})
	}

	if($('.form-control').hasClass('select2')){
		$('.select2').select2({ width: 'resolve' });
	}
})