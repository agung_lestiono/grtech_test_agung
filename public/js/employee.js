$(document).ready(function() {	
	if($('.form-control').hasClass('select2'))
		$('.select2').select2({ width: 'resolve' });

    $('#employee-table').on('click', '.edit-btn', function(e) {
    	$.get('/employees/edit/' + $(this).data('employee-id'), function (data) {
    		$('#id').val(data.id);
    		$('#first-name').val(data.first_name);
    		$('#last-name').val(data.last_name);
    		$('#company-id').val(data.company_id);
    		$("#company-id").val(data.company_id).trigger('change') ;
    		$('#email').val(data.email);
    		$('#phone').val(data.phone);
    	})
    })

  //   $('#update-btn').click(function (e) {
  //   	e.preventDefault();
  //   	$.ajaxSetup({
  //           headers: {
  //               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  //           }
  //       });

  //   	$.post('/employees/update/' + $('#id').val(), {
		// 	first_name : $('#first-name').val(),
		// 	last_name : $('#last-name').val(),
		// 	company_id : $('#company-id').val(),
		// 	email : $('#email').val(),
		// 	phone : $('#phone').val(),
		// }).done(function (result){
		// 	window.location.href = '/employees';
		// })
  //   })
});